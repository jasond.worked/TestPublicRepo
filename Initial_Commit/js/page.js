$(document).ready(function (e) {
  
  
    function showView(viewName) {
        $('.view').hide();
    $('#' + viewName).show();
    }

    $('[launch]').click(function (e) {
      e.preventDefault();
      var viewName = $(this).attr('launch');
      showView(viewName);
    });
});